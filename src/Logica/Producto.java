/**
 *
 * @author Cesar Andres Diaz Delgado
 * concido en el bajo mundo como K4TheX
 */
package Logica;

import Interfaz.Interfaz;

public class Producto {

    private String nombre;
    private String id;
    private double temperatura;
    private double valorBase;

    public Producto() {
    }

    public Producto(String nombre, String id, double temperatura, double valorBase) {
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Interfaz p = new Interfaz();
        p.setVisible(true);
    }

}
