/**
 *
 * @author Cesar Andres Diaz Delgado concido en el bajo mundo como K4TheX
 */
package Vinculo;

import java.sql.*;

public class BD {

    Connection conect = null;
    String url = "jdbc:sqlite:reto5_2.db";

    public Connection conectarBaseDatos() {
        try {
            conect = DriverManager.getConnection(url);
        } catch (Exception e) {
            System.out.printf(e.getMessage());
        }
        return conect;
    }
}
